require "byebug"
class Hangman
  attr_reader :guesser, :referee, :board, :dictionary, :secret_word_length

  def initialize(players = {})
    @dictionary = File.readlines("dictionary.txt").map(&:chomp)
    default = {
      guesser: ComputerPlayer.new(@dictionary),
      referee: ComputerPlayer.new(@dictionary)
    }
    players = default.merge(players)
    @guesser, @referee = players[:guesser], players[:referee]
    @secret_word_length = 0
    @number_of_guesses_left = 10
  end

  def play
    setup
    until game_over?
      # setup the game; referee picks a secret word; guesser registers the length
      # display board
      display
      # take the turn
      take_turn
    end
    puts "Game over!"
    puts "The secret word was #{referee.secret_word}"
  end

  def guess_the_word(word)
    if referee.secret_word.downcase == word.downcase
      puts "Correct! You guessed right! The secret word was #{referee.secret_word}."
      exit
    else
      puts "Incorrect! Very sad!"
    end
  end

  def game_over?
    if @board.join.downcase == referee.secret_word.downcase
      puts "Guesser wins!"
      return true
    elsif @number_of_guesses_left < 1
      puts  "Guesser loses!"
      return true
    else
      return false
    end
  end

  def display
    puts "Guesser has #{@number_of_guesses_left} guesses left!"
    @board.each do |letter|
      disp_space = ""
      letter == nil ? disp_space = "_" : disp_space = letter
      print "#{disp_space}  "
    end
    print "         Guessed Letters: "
    guesser.guessed_letters.uniq.each { |let| print " #{let} " }
    puts ""
  end

  def setup
    # guesser.register_secret_length(referee.pick_secret_word)
    @secret_word_length = referee.pick_secret_word
    @guesser.register_secret_length(@secret_word_length)
    @board = [nil] * @secret_word_length
  end

  def take_turn
    puts "Final turn! You should probably guess a word..." if @number_of_guesses_left == 1
    new_guess = @guesser.guess(@board)
    if new_guess.length > 1
      guess_the_word(new_guess)
    else
      correctly_guessed_indices = @referee.check_guess(new_guess)
      update_board(new_guess, correctly_guessed_indices)
      @guesser.handle_response(new_guess, correctly_guessed_indices)
    end
    @number_of_guesses_left -= 1

  end

  def update_board(letter, indices)
    indices.each { |index| @board[index] = letter.downcase }
  end
end

class ComputerPlayer

  def initialize(dictionary)
    @dictionary = dictionary ## @dictionary should be an array! (I think)
    @secret_word = ""
    @secret_word_length = 0
    @board = []
    @guessed_letters = []
  end

  attr_reader :secret_word_length, :secret_word, :guessed_letters

  def pick_secret_word
    @secret_word = @dictionary[rand(0...@dictionary.length)]
    @secret_word.length
  end

  def check_guess(letter)
    indices = []
    @secret_word.chars.each_with_index { |let, index| indices << index if let == letter }
    indices
  end

  def register_secret_length(length)
    @secret_word_length = length
    @board = [nil] * length
    @secret_word_length
  end

  def guess(board)
    @board = board
    @board.each { |letter| @guessed_letters << letter if letter }
    @guessed_letters.uniq!
    # count the frequencies of letters out of all the remaining words
    frequency_hash = Hash.new(0)
    candidate_words.each do |word|
      word.chars.each { |letter| frequency_hash[letter] += 1 }
    end

    # finally, find the letter that appears most in the frequency hash and return it
    sorted_arr = frequency_hash.sort_by { |key, val| val }
    sorted_arr.reverse!
    new_guessed_letter = false
    most_frequent_letter_index = 0
    until new_guessed_letter
      guess_letter = sorted_arr[most_frequent_letter_index][0]
      if @guessed_letters.include?(guess_letter)
        most_frequent_letter_index += 1
      else
        new_guessed_letter = true
      end
    end
    @guessed_letters << guess_letter
    return guess_letter
  end

  def add_guessed_letter(letter)
    @guessed_letters << letter
  end

  def candidate_words
    # whittle down the dictionary array into posible words given the board
    # "rear" ---- R, nil, nil, nil [0]
    possible_words = @dictionary.dup
    possible_words.reject! { |word| word.length != @secret_word_length }
    @board.each_with_index do |letter, index|
      if letter != nil
        possible_words.reject! { |word| word[index] != letter }
      else # (i.e. letter == nil)
        possible_words.reject! { |word| @guessed_letters.include?(word[index]) }
      end
    end
    possible_words

    ## PROBLEM: if 'r' appears in the first and fourth index, this alg might not eliminate a word like "rear"
    # make make a hash with indices to represent the board?

  end

  def handle_response(letter, indices)
    times = indices.length
    if times > 1
      puts "Nice! There are #{times} instances of #{letter} in the secret word."
    elsif times == 1
      puts "Nice! There is one 1 instance of #{letter} in the secret word."
    else
      puts "Dang! There are no #{letter}\'s in the secret word."
    end
    # update the board
    indices.each { |idx| @board[idx] = letter }
    @guessed_letters << letter
  end
end

class HumanPlayer < ComputerPlayer

  def pick_secret_word
    picked_word = ""
    until @dictionary.include?(picked_word)
      puts "Pick (a real!) secret word: "
      picked_word = gets.chomp.downcase
    end
    puts "You picked #{picked_word}. Good on you!"
    @secret_word = picked_word
    @secret_word.length
  end

  def guess(board)
    @board = board
    @board.each { |letter| @guessed_letters << letter if letter }
    guess_letter = ""
    until (("a".."z").to_a.include?(guess_letter) && !@guessed_letters.include?(guess_letter)) || guess_letter.length == @secret_word_length
      puts "Guess a letter or a #{@secret_word_length}-letter word: "
      guess_letter = gets.chomp.downcase
      if guess_letter.length == 1
        puts "Bad! You already guessed that letter. Guess again: " if @guessed_letters.include?(guess_letter)
        puts "Bad! That's not a letter. Guess a letter or a word: " unless ("a".."z").to_a.include?(guess_letter)
      elsif guess_letter.length != @secret_word_length
        puts "Bad! That's not the right length. Guess again: "
      else
      end
    end
    @guessed_letters << guess_letter if guess_letter.length == 1
    guess_letter
  end


end

# =begin
if __FILE__ == $PROGRAM_NAME
  dictionary = File.readlines("dictionary.txt").map(&:chomp)
  players = {
    guesser: ComputerPlayer.new(dictionary),
    referee: HumanPlayer.new(dictionary)
  }
  game = Hangman.new(players)
  game.play
end
# =end
